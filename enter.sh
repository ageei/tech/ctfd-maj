#!/bin/sh
set -eu

n="${1}"
shift

DIRNAME=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)
DIRNAME=$(basename -- "${DIRNAME}")
set -x
exec docker exec -it "${DIRNAME}_${n}_1" /bin/bash "${@}"
