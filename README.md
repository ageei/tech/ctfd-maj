# Migration de la DB du CTFd

La base de données du CTFd utilise présentement PostgreSQL. Cette
dépendance nous oblige à gérer un système additionnel et nuit aux tests
automatisés. Ce projet vise à documenter la procédure de migration du
CTFd à (1) une version plus récente et (2) de PostgreSQL à SQLite.

## Approche

1. Lancer la version courante du CTFd dans Docker.
2. Lancer une instance de PostgreSQL dans Docker.
3. Importer la base de données courante dans l'instance de PostgreSQL.
4. Configurer CTFd pour que ça utilise la base de données.
5. Vérifier le bon fonctionnement du CTFd.
6. Mettre à jour le CTFd.
7. Voir comment effectuer la migration de PostgreSQL à SQLite.

## Notes

- Il faut importer la db de CTFd avant de démarrer CTFd.
- Ensuite, il faut mettre a jour le theme via la db pour que
  l'app soit accessible (le theme ageei n'est pas installer).
- Vider le tableau tracking durant la sauvegarde.
- Les exercices Pantin et GOT doivent être masqués avant la sauvegarde.
- Around the glob doit être masqué avant la sauvegarde.
- pg_dump -U ctfd >/mnt/new.sql

## Notes de déploiement

Tout est aller numéro 1.

- Mise hors-ligne du CTF.
- Arrêt du ctfd.
- Export de la db.
- Migration de la db.
- Renommage de la base de données (`ALTER DATABASE ctfd RENAME TO ctfd_old`).
- Création de la nouvelle base de données (`CREATE DATABASE ctfd WITH OWNER ctfd`).
- Import de la db.
- Mise à jour du ctfd.
- Redémarrage du ctfd.
- Tests de connectivité.
- Vérification des sauvegardes.
