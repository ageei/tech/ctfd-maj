set -eu

cmd() {
	sh enter.sh ctfd -c "${@}"
}

up() {
	docker-compose up -d "${1}"
	sleep 1
	while :; do
		r=$(docker ps --format '{{.Names}} {{.Status}}' --filter "name=_${1}_" | cut -d' ' -f2- | grep -o '[0-9].*')
		n=$(echo "${r}" | cut -d' ' -f1)
		u=$(echo "${r}" | cut -d' ' -f2)
		case "${u}" in
		minutes)
			break
			;;
		*)
			[ "$n" -le 5 ] || break
			;;
		esac
		sleep 1
	done
}

dn() {
	docker-compose stop "${1}"
	while :; do
		if ! docker ps | grep -q "_${1}_"; then
			break
		fi
	done
}

re() {
	dn "${1}"
	up "${1}"
}


up db
sh enter.sh db -c 'psql -U ctfd < /mnt/ctfd.sql'
sh enter.sh db -c "psql -U ctfd -c \"UPDATE config SET value = 'original' WHERE key = 'ctf_theme'\""
up ctfd

cmd	"git checkout -bv1.1.0 tags/1.1.0"
cmd	"pip install -r requirements.txt"
dn ctfd
docker-compose up -d ctfd
sleep 2
up ctfd


for v in 1.1.1 1.1.2 1.1.3 1.1.4 1.2.0; do
	cmd	"git checkout -b 'v${v}' 'tags/${v}'"
	cmd	'pip install -r requirements.txt'
	re ctfd
done

# 2.0.0
cmd	'git checkout -bv2.0.0 tags/2.0.0'
cmd	'pip install -r requirements.txt'
cmd	"sh -c 'sed -i -e \"s|import os|import os,six|\" migrations/1_2_0_upgrade_2_0_0.py'"
cmd	"sh -c 'yes | python migrations/1_2_0_upgrade_2_0_0.py'"
cmd	'git checkout migrations/1_2_0_upgrade_2_0_0.py'
re ctfd

for v in 2.0.1 2.0.2 2.0.3 2.0.4 2.0.5 2.0.6 2.1.0 2.1.1 2.1.2 2.1.3 2.1.4 2.1.5 2.2.0 2.2.1 2.2.2 2.2.3 2.3.0 2.3.1; do
	cmd	"git checkout -b 'v${v}' 'tags/${v}'"
	cmd	'pip install -r requirements.txt'
	re ctfd
done
